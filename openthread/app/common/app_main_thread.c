/*
* Copyright 2019 NXP
* All rights reserved.
*
* SPDX-License-Identifier: BSD-3-Clause
*/

/*!=================================================================================================
 \file       app_init.c
 \brief      This is a public source file for the initial system startup module. It contains
 the implementation of the interface functions.
 ==================================================================================================*/

/*==================================================================================================
 Include Files
 ==================================================================================================*/
#include "fsl_device_registers.h"
#include "fsl_os_abstraction.h"
#if WDOG_ENABLE
#include "fsl_wdog.h"
#endif

#include "app_init.h"

/* FSL Framework */
#include "RNG_Interface.h"
#include "LED.h"
#include "MemManager.h"

#include "TimersManager.h"
#include "Keyboard.h"
#if gLpmIncluded_d
#include "PWR_Interface.h"
#endif

#include "fsl_wtimer.h"
#include "Panic.h"
#include "SecLib.h"
#include "app_init.h"
#include "board.h"
#include "network_utils.h"
#include "FunctionLib.h"

#include <openthread-system.h>
#include <openthread/instance.h>
#if OT_USE_CLI
#include <openthread/cli.h>
#elif OT_USE_SPINEL
#include <openthread/ncp.h>
#endif
#include <openthread/tasklet.h>
#include <openthread/platform/radio.h>

extern void APP_Init(void);
extern void vMMAC_IntHandlerBbc();
extern void vMMAC_IntHandlerPhy();

/*==================================================================================================
 Private macros
 ==================================================================================================*/

/*==================================================================================================
 Private type definitions
 ==================================================================================================*/

/*==================================================================================================
 Private prototypes
 ==================================================================================================*/
static void KBD_Callback(uint8_t events);

/*==================================================================================================
 Private global variables declarations
 ==================================================================================================*/

/*==================================================================================================
 Public global variables declarations
 ==================================================================================================*/
uint32_t eventMask = 0;

/*==================================================================================================
 Public functions
 ==================================================================================================*/
void main_task(uint32_t param)
{
    static uint8_t mainInitialized = FALSE;

    if (!mainInitialized)
    {
        mainInitialized = TRUE;

#if WDOG_ENABLE
        /* Init watchdog module */
        APP_WDOG_Init();
#endif
        /* Init memory blocks manager */
        MEM_Init();

        SecLib_Init();
        /* RNG software initialization and PRNG initial seeding (from hardware) */
        if(RNG_Init() != gRngSuccess_d)
        {
            panic(0, (uint32_t)main_task, 0, 0);
        }

        RNG_SetPseudoRandomNoSeed(NULL);
        
        /* Init  timers module */
        TMR_Init();
        TMR_TimeStampInit();

        /* Install uMac interrupt handlers */
        OSA_InstallIntHandler(ZIGBEE_MAC_IRQn, vMMAC_IntHandlerBbc);
        OSA_InstallIntHandler(ZIGBEE_MODEM_IRQn, vMMAC_IntHandlerPhy);

        /* Initialize OpenThread Module */
        OT_Init();

#if gLEDSupported_d
        /* Init Led module */
        LED_Init();
#endif

#if gLpmIncluded_d
        PWR_Init();
        PWR_DisallowDeviceToSleep();
        PWR_ChangeDeepSleepMode(3);
#endif
        /* Initialize Keyboard (Switches) Module */
        KBD_Init(KBD_Callback);

        /* Init demo application */
        APP_Init();
    }

    /* Main Application Loop (idle state) */
    App_OT_Thread(param);
}

/*==================================================================================================
 Private functions
 ==================================================================================================*/
/*!*************************************************************************************************
 \fn  static void KBD_Callback(uint8_t events)
 \brief  This is a callback function called from the KBD module.

 \param  [in]    events  value of the events

 \return         void
 ***************************************************************************************************/
static void KBD_Callback(uint8_t events)
{
    /* memory optimisation - app keyboard handler handles the pointer as an events mask*/
    eventMask = eventMask | (uint32_t) (1 << events);
#if gLpmIncluded_d
    /* Prevent sleep until keyboard event is proceed by the thread APP_Handler */
    PWR_PreventEnterLowPower(TRUE);
#endif
}

/*==================================================================================================
 Private debug functions
 ==================================================================================================*/
