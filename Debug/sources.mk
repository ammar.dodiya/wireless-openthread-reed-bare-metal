################################################################################
# Automatically-generated file. Do not edit!
################################################################################

OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
board \
component/lists \
component/serial_manager \
component/uart \
device \
drivers \
framework/Common \
framework/Flash/External/source \
framework/Flash \
framework/FunctionLib/source \
framework/GPIO/source \
framework/Keyboard/source \
framework/LED/source \
framework/Lists/source \
framework/MemManager/source \
framework/Messaging/source \
framework/ModuleInfo/source \
framework/OSAbstraction/source \
framework/OtaSupport/source \
framework/PDM/source \
framework/Panic/source \
framework/RNG/source \
framework/Reset/source \
framework/SecLib/source \
framework/SerialManager/source/SPI_Adapter \
framework/SerialManager/source \
framework/TimersManager/source \
openthread/app/common \
source \
startup \
utilities \

