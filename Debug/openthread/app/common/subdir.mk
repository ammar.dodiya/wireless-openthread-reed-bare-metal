################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../openthread/app/common/app_init.c \
../openthread/app/common/app_latency_measure.c \
../openthread/app/common/app_led.c \
../openthread/app/common/app_main_thread.c \
../openthread/app/common/app_ota_client.c \
../openthread/app/common/missing_strlcpy.c \
../openthread/app/common/network_utils.c 

OBJS += \
./openthread/app/common/app_init.o \
./openthread/app/common/app_latency_measure.o \
./openthread/app/common/app_led.o \
./openthread/app/common/app_main_thread.o \
./openthread/app/common/app_ota_client.o \
./openthread/app/common/missing_strlcpy.o \
./openthread/app/common/network_utils.o 

C_DEPS += \
./openthread/app/common/app_init.d \
./openthread/app/common/app_latency_measure.d \
./openthread/app/common/app_led.d \
./openthread/app/common/app_main_thread.d \
./openthread/app/common/app_ota_client.d \
./openthread/app/common/missing_strlcpy.d \
./openthread/app/common/network_utils.d 


# Each subdirectory must supply rules for building sources it contributes
openthread/app/common/%.o: ../openthread/app/common/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DCPU_K32W061HN -DCPU_K32W061HN_cm4 -DCPU_K32W061 -DSDK_DEBUGCONSOLE=1 -DCR_INTEGER_PRINTF -DDK6 -DSDK_DEVICE_FAMILY=K32W061 -DSDK_BOARD=k32w061dk6 -DJENNIC_CHIP_FAMILY_NAME=_JN518x -DCPU_JN518X -DENABLE_RAM_VECTOR_TABLE=1 -D__NEWLIB__ -DSUPPORT_FOR_15_4=1 -DSUPPORT_FOR_BLE=0 -DPRINTF_FLOAT_ENABLE=1 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/board" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/source" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/OSAbstraction/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Flash" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/GPIO/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Keyboard/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/LED/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Common" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/PDM/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/MemManager/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Messaging/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Panic/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/RNG/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/TimersManager/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/TimersManager/source" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/ModuleInfo/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/FunctionLib/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Lists/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SecLib/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/OtaSupport/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SerialManager/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SerialManager/source" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/XCVR/DK6" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/ieee-802.15.4/uMac/Include" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/interface/openthread" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/interface/openthread/platform" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/app/common" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/app/common/utils" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/drivers" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/device" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/component/lists" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/component/uart" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/component/serial_manager" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/utilities" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/CMSIS" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Flash/External/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SerialManager/source/SPI_Adapter" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/interface" -O0 -fno-common -g3 -Wall -Wno-missing-braces  -c  -ffunction-sections  -fdata-sections  -ffreestanding  -fno-builtin -imacros "/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/source/config.h" -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mthumb -D__NEWLIB__ -fstack-usage -specs=nano.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


