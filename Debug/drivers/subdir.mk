################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/fsl_adc.c \
../drivers/fsl_aes.c \
../drivers/fsl_clock.c \
../drivers/fsl_common.c \
../drivers/fsl_ctimer.c \
../drivers/fsl_flash.c \
../drivers/fsl_flexcomm.c \
../drivers/fsl_fmeas.c \
../drivers/fsl_gpio.c \
../drivers/fsl_inputmux.c \
../drivers/fsl_pint.c \
../drivers/fsl_power.c \
../drivers/fsl_reset.c \
../drivers/fsl_rng.c \
../drivers/fsl_rtc.c \
../drivers/fsl_sha.c \
../drivers/fsl_spi.c \
../drivers/fsl_spifi.c \
../drivers/fsl_usart.c \
../drivers/fsl_wtimer.c 

OBJS += \
./drivers/fsl_adc.o \
./drivers/fsl_aes.o \
./drivers/fsl_clock.o \
./drivers/fsl_common.o \
./drivers/fsl_ctimer.o \
./drivers/fsl_flash.o \
./drivers/fsl_flexcomm.o \
./drivers/fsl_fmeas.o \
./drivers/fsl_gpio.o \
./drivers/fsl_inputmux.o \
./drivers/fsl_pint.o \
./drivers/fsl_power.o \
./drivers/fsl_reset.o \
./drivers/fsl_rng.o \
./drivers/fsl_rtc.o \
./drivers/fsl_sha.o \
./drivers/fsl_spi.o \
./drivers/fsl_spifi.o \
./drivers/fsl_usart.o \
./drivers/fsl_wtimer.o 

C_DEPS += \
./drivers/fsl_adc.d \
./drivers/fsl_aes.d \
./drivers/fsl_clock.d \
./drivers/fsl_common.d \
./drivers/fsl_ctimer.d \
./drivers/fsl_flash.d \
./drivers/fsl_flexcomm.d \
./drivers/fsl_fmeas.d \
./drivers/fsl_gpio.d \
./drivers/fsl_inputmux.d \
./drivers/fsl_pint.d \
./drivers/fsl_power.d \
./drivers/fsl_reset.d \
./drivers/fsl_rng.d \
./drivers/fsl_rtc.d \
./drivers/fsl_sha.d \
./drivers/fsl_spi.d \
./drivers/fsl_spifi.d \
./drivers/fsl_usart.d \
./drivers/fsl_wtimer.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/%.o: ../drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DCPU_K32W061HN -DCPU_K32W061HN_cm4 -DCPU_K32W061 -DSDK_DEBUGCONSOLE=1 -DCR_INTEGER_PRINTF -DDK6 -DSDK_DEVICE_FAMILY=K32W061 -DSDK_BOARD=k32w061dk6 -DJENNIC_CHIP_FAMILY_NAME=_JN518x -DCPU_JN518X -DENABLE_RAM_VECTOR_TABLE=1 -D__NEWLIB__ -DSUPPORT_FOR_15_4=1 -DSUPPORT_FOR_BLE=0 -DPRINTF_FLOAT_ENABLE=1 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/board" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/source" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/OSAbstraction/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Flash" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/GPIO/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Keyboard/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/LED/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Common" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/PDM/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/MemManager/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Messaging/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Panic/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/RNG/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/TimersManager/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/TimersManager/source" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/ModuleInfo/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/FunctionLib/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Lists/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SecLib/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/OtaSupport/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SerialManager/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SerialManager/source" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/XCVR/DK6" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/ieee-802.15.4/uMac/Include" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/interface/openthread" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/interface/openthread/platform" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/app/common" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/app/common/utils" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/drivers" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/device" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/component/lists" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/component/uart" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/component/serial_manager" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/utilities" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/CMSIS" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/Flash/External/interface" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/framework/SerialManager/source/SPI_Adapter" -I"/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/openthread/interface" -O0 -fno-common -g3 -Wall -Wno-missing-braces  -c  -ffunction-sections  -fdata-sections  -ffreestanding  -fno-builtin -imacros "/home/ammar/Embed/Pre-thesis/Project_example/k32w061dk6_reed_bm/source/config.h" -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mthumb -D__NEWLIB__ -fstack-usage -specs=nano.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


